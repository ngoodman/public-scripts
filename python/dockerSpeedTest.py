import argparse
import subprocess
import time
import sys
import re

def image_exists(image_name):
    result = subprocess.run(['docker', 'image', 'inspect', image_name], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return result.returncode == 0

def remove_docker_image(image_name):
    try:
        subprocess.run(['docker', 'rmi', image_name], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError as e:
        print(f"Error removing image {image_name}: {e}")
        sys.exit(1)

def pull_docker_image(image_name):
    layer_start_times = {}
    layer_end_times = {}

    process = subprocess.Popen(['docker', 'pull', image_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            print(output.strip())
            layer_id_search = re.search(r'([a-f0-9]+): (?:Downloading|Pulling)', output)
            if layer_id_search:
                layer_id = layer_id_search.group(1)
                if layer_id not in layer_start_times:
                    layer_start_times[layer_id] = time.perf_counter()

            layer_complete_search = re.search(r'([a-f0-9]+): Download complete', output)
            if layer_complete_search:
                layer_id = layer_complete_search.group(1)
                layer_end_times[layer_id] = time.perf_counter()

    total_elapsed_time = 0
    for layer_id in layer_start_times:
        if layer_id in layer_end_times:
            total_elapsed_time += layer_end_times[layer_id] - layer_start_times[layer_id]

    return total_elapsed_time


def get_image_size(image_name):
    result = subprocess.run(['docker', 'image', 'inspect', image_name, '--format', '{{.Size}}'], capture_output=True)
    if result.returncode == 0:
        return int(result.stdout.strip())
    else:
        print(f"Error getting image size for {image_name}: {result.stderr}")
        sys.exit(1)

def main(args):
    image_name = args.image

    if image_exists(image_name):
        print(f"The image '{image_name}' is already present on your machine.")
        print("For accurate bandwidth calculation, it is recommended to remove the Docker image if it already exists on your machine.")
        print("This ensures that the image is pulled from the remote repository.")
        remove_image = input(f"Do you want to remove the image '{image_name}'? (y/n): ").lower()

        if remove_image == 'y':
            # Remove the image if it already exists
            remove_docker_image(image_name)
        else:
            print("Exiting...")
            sys.exit(0)

    total_elapsed_time = pull_docker_image(image_name)

    if total_elapsed_time is not None:
        image_size = get_image_size(image_name)
        image_size_mb = image_size / (1024 * 1024)

        bandwidth = image_size_mb / total_elapsed_time

        print(f"Bandwidth: {bandwidth:.2f} MB/s")
    else:
        print("Error: Unable to measure download time.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Docker image download bandwidth calculator')
    parser.add_argument('--image', type=str, required=True, help='Docker image to download (e.g., alpine:latest)')

    args = parser.parse_args()

    if args.image:
        main(args)
    else:
        print("Please provide an image name using --image flag.")
        sys.exit(1)
