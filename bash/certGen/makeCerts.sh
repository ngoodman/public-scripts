#!/bin/bash

# Dependency Check
for d in yq jq sops openssl keytool ; do which "$d" > /dev/null || { echo "Missing Dependency: $d"; exit 1; }; done

# Generate a PEM private key
function generate_private_key() {
  local CN="$1"
  local CERT_PASS="$2"

  openssl genrsa -aes256 -out "${CN}".key 2048 -passout pass:"$CERT_PASS"
    if [ ! -f "${CN}.key" ] ; then
      echo "Failed to generate private key: $CN.key"
      echo "Exiting..."
      exit 1
    fi
}

# Generate a PEM certificate
function generate_certificate() {
  local CN="$1"
  local C="$2"
  local ST="$3"
  local L="$4"
  local O="$5"
  local OU="$6"
  local SAN="$7"
  local CERT_PASS="$8"
  local SIGNING_CERT="$9"
  local SIGNING_KEY="${10}"
  local SIGNING_PASS="${11}"
  local ROOT_PUBLIC="${12}"

  openssl req -new -key "${CN}.key" -out "${CN}.csr" -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/OU=${OU}/CN=${CN}" -passin pass:"$CERT_PASS"
    if [ ! -f "${CN}.csr" ] ; then
      echo "Failed to generate csr: $CN.csr"
      echo "Exiting..."
      exit 1
    fi

  # Join the DNS and IP values with a newline character
  DNS_ENTRIES=$(echo "$SAN" | jq -r '.DNS | to_entries[] | "DNS.\(.key + 1)=\(.value)"')
  IP_ENTRIES=$(echo "$SAN" | jq -r '.IP | to_entries[] | "IP.\(.key + 1)=\(.value)"')
  SAN_LIST="${DNS_ENTRIES}"$'\n'"${IP_ENTRIES}"


  # Create an openssl.cnf file with the necessary extended key usage configuration
  cat > openssl.cnf <<- EOM
[req]
distinguished_name = req_distinguished_name
req_extensions = v3_req
[req_distinguished_name]
[v3_req]
basicConstraints = CA:FALSE
keyUsage = digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth, clientAuth
subjectAltName = @alt_names
[alt_names]
$SAN_LIST
EOM

  openssl x509 -req -in "${CN}".csr -CA "${SIGNING_CERT}" -CAkey "${SIGNING_KEY}" -CAcreateserial -out "${CN}".pem -days 365 -extfile openssl.cnf -extensions v3_req -passin pass:"$SIGNING_PASS"
   if [ ! -f "${CN}.pem" ] ; then
      echo "Failed to generate pem: $CN.pem"
      echo "Exiting..."
      exit 1
    else
      rm "${CN}.csr"
      rm openssl.cnf
    fi


  # Concatenate the PEM certificate with the Intermediate and Root CAs to create the full chain of trust
  cat "${CN}".pem "${SIGNING_CERT}" "${ROOT_PUBLIC}" > "${CN}-chain.pem"
}

# Generate a P12 file
function generate_p12() {
  local CN="$1"

  openssl pkcs12 -export -in "${CN}"-chain.pem -inkey "${CN}".key -out "${CN}".p12 -name "${CN}" -passin pass:"$CERT_PASS" -password pass:"$CERT_PASS"
      if [ ! -f "${CN}.p12" ] ; then
      echo "Failed to generate p12: $CN.p12"
      echo "Exiting..."
      exit 1
    fi
}

# Generate a JKS file
function generate_jks() {
  local CN="$1"

  keytool -importkeystore -srckeystore "${CN}".p12 -srcstoretype PKCS12 -destkeystore "${CN}".jks -deststoretype JKS -srcstorepass "$CERT_PASS" -deststorepass "$CERT_PASS"
  if [ ! -f "${CN}.jks" ] ; then
      echo "Failed to generate jks: $CN.jks"
      echo "Exiting..."
      exit 1
    fi
}

# Decrypt YAML file
# sops -d certs-enc.yaml > certs.yaml

# Read values from the YAML file
certs=$(cat certs.yaml | yq .certs -o json)

for cert in $(echo "${certs}" | jq -r '.[] | @base64'); do
  cert_json=$(echo "${cert}" | base64 --decode | jq -r '.')
  CN=$(echo "${cert_json}" | jq -r '.CN')
  SAN=$(echo "${cert_json}" | jq -r '.SAN | tojson')
  C=$(echo "${cert_json}" | jq -r '.Subject.Country')
  ST=$(echo "${cert_json}" | jq -r '.Subject.State')
  L=$(echo "${cert_json}" | jq -r '.Subject.Locality')
  O=$(echo "${cert_json}" | jq -r '.Subject.Organization')
  OU=$(echo "${cert_json}" | jq -r '.Subject.OrganizationalUnit')
  CERT_PASS=$(echo "${cert_json}" | jq -r '.Password')
  SIGNING_CERT=$(echo "${cert_json}" | jq -r '.CertAuthorities.SigningCertPublic')
  SIGNING_KEY=$(echo "${cert_json}" | jq -r '.CertAuthorities.SigningCertPrivate')
  SIGNING_PASS=$(echo "${cert_json}" | jq -r '.CertAuthorities.SigningCertPassword')
  ROOT_PUBLIC=$(echo "${cert_json}" | jq -r '.CertAuthorities.RootCertAuthorityPublic')

  if [ -d "${CN}" ] ; then
    echo "Directory for $CN already exists. Skipping certificate generation."
  else
    generate_private_key "$CN" "$CERT_PASS"
    generate_certificate "$CN" "$C" "$ST" "$L" "$O" "$OU" "$SAN" "$CERT_PASS" "$SIGNING_CERT" "$SIGNING_KEY" "$SIGNING_PASS" "$ROOT_PUBLIC"
    generate_p12 "$CN"
    generate_jks "$CN"
    mkdir "$CN"
    mv "$CN".key "$CN"/"$CN".key
    mv "$CN".pem "$CN"/"$CN".pem
    mv "$CN"-chain.pem "$CN"/"$CN"-chain.pem
    mv "$CN".p12 "$CN"/"$CN".p12
    mv "$CN".jks "$CN"/"$CN".jks

    echo "Files generated for $CN:"
    echo "  - $CN/$CN.key"
    echo "  - $CN/$CN.pem"
    echo "  - $CN/$CN-chain.pem"
    echo "  - $CN/$CN.p12"
    echo "  - $CN/$CN.jks"
  fi
done
# Remove decrypted YAML file
# rm -rf certs.yaml