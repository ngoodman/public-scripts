#!/bin/bash

# Function to create a RootCA
create_root_ca() {
  echo "Creating RootCA..."

  # Prompt the user for a password to generate the RootCA private key
  read -s -p "Enter a password for the RootCA private key: " root_ca_password
  echo

  # Create the RootCA private key
  openssl genpkey -algorithm RSA -out Kube_Root_Test_CA1.key -aes256 -pass pass:$root_ca_password

  # Create the RootCA certificate
  openssl req -new -x509 -sha256 -key Kube_Root_Test_CA1.key -out Kube_Root_Test_CA1.crt -days 3650
}

# Function to create an Intermediate CA
create_intermediate_ca() {
  echo "Creating Intermediate CA..."

  # Prompt the user for a password to generate the Intermediate CA's private key
  read -s -p "Enter a password for the Intermediate CA private key: " intermediate_ca_password
  echo

  # Create the Intermediate CA private key
  openssl genpkey -algorithm RSA -out Kube_Intermediate_Test_CA1.key -aes256 -pass pass:$intermediate_ca_password

  # Create the Intermediate CA CSR
  openssl req -new -sha256 -key Kube_Intermediate_Test_CA1.key -out Kube_Intermediate_Test_CA1.csr

  # Sign the Intermediate CA CSR with the RootCA certificate
  openssl x509 -req -in Kube_Intermediate_Test_CA1.csr -CA Kube_Root_Test_CA1.crt -CAkey Kube_Root_Test_CA1.key -CAcreateserial -out Kube_Intermediate_Test_CA1.crt -days 1825 -sha256
}

# Call the create_root_ca function
create_root_ca

# Call the create_intermediate_ca function
create_intermediate_ca